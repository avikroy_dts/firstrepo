//
//  SWAppDelegate.h
//  testgithub
//
//  Created by Dreamz Tech Solution on 02/09/13.
//  Copyright (c) 2013 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWViewController;

@interface SWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SWViewController *viewController;

@end
