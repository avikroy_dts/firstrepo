//
//  SWTestViewController.m
//  testgithub
//
//  Created by Dreamz Tech Solution on 02/09/13.
//  Copyright (c) 2013 Company. All rights reserved.
//

#import "SWTestViewController.h"

@interface SWTestViewController ()

@end

@implementation SWTestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
