//
//  main.m
//  testgithub
//
//  Created by Dreamz Tech Solution on 02/09/13.
//  Copyright (c) 2013 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SWAppDelegate class]));
    }
}
